/*Number 1*/
let count = Number(prompt("Give me a number"));
console.log("The number you provided is: " +count);

for(count; count >=50 ; count--){

	if(count % 10 ===0){
		console.log("The number is divisible by 10. Skipping the number.");
		continue;
	}

	if (count % 5 === 0) {
		console.log(count);
	}

	if (count<50)
	{	
		console.log("The current value is at 50. Terminating the loop.");
		break;
	}
}

/*Number 2*/

let myName = "supercalifragilisticexpialidocious";
console.log(myName);
let newStr="";
for(let i = 0; i < myName.length; i++){
		
	if(
		myName[i] == "a" ||
		myName[i] == "e" ||
		myName[i] == "i" ||
		myName[i] == "o" ||
		myName[i] == "u" 
	  )
	{	
		continue;
	}

	else{
		
		newStr += myName[i]
	}

}
console.log(newStr);

